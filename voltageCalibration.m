voltage = load('voltage_HSn_16022022_coil10.mat')
amplitude = load('amplitude_HSn_16022022_coil10.mat')

%voltage2 = load('voltage_Hard_01012022.mat')
%amplitude2 = load('amplitude_Hard_01012022.mat')

voltage = cell2mat(struct2cell(voltage))
amplitude = cell2mat(struct2cell(amplitude))

%voltage2 = cell2mat(struct2cell(voltage2))
%amplitude2 = cell2mat(struct2cell(amplitude2))

%print a colormap of the intensity of the pixels
figure();
imagesc(abs(amplitude(:,:,3).'))
axis square;
colorbar;

clear voltage1
clear amplitude1
%to order the voltages into ascending order
voltage1 = sort(voltage)
for i=1:length(voltage1)
    for j=1:length(voltage)
        if voltage1(i) == voltage(j)
            amplitude1(:,:,i) = amplitude(:,:,j)
        end
    end
end

%to order the voltages into ascending order
% voltage3 = sort(voltage2)
% for i=1:length(voltage3)
%     for j=1:length(voltage2)
%         if voltage3(i) == voltage2(j)
%             amplitude3(:,:,i) = amplitude2(:,:,j)
%         end
%     end
% end

% create a grid of the pixel subplots
n=0;
figure();
for i = 1:8
    for j=1:8
        n = n+1;
        subplot(8,8,n);
        axis('equal')
        plot(voltage1, squeeze(amplitude1(j,i,:)), 'x');
    end
end

%print each individual pixel graph in turn
for i=1:8
    for j =1:8
        
        figure()
        hold on
        plot(voltage1, squeeze(amplitude1(i,j,:)),'-bs')
        %plot(voltage3, squeeze(amplitude3(i,j,:)),'-ks')
        title(['Voltage Calibration Curve for Pixel ' num2str(i) ',' num2str(j)])
        xlabel('voltage/V')
        ylabel('amplitude')
        ylim([0 inf])
        %legend('HSn', 'Hard')
        hold off
        
        %saveas(gcf,['C:\Users\hseab\Documents\ProjectIIIMatlabCode\OldCodeCalibration\Plots_HSn_16.02.2022\pixel' num2str(i) num2str(j) '.png'])
    end
end


