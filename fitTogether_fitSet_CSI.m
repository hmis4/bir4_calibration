function [fitRes] = fitTogether_fitSet_CSI(dt, fitParam)

seriesList = dt.search('target','series','return','series',...
    'query',@(ser,stu) ...
     any(regexp(ser.SeriesDescription,fitParam.regexp,'once')));
%     any(regexp(ser.SeriesDescription,'fid_jk_P2_400V_[0-9]+ms_uc_upw','once')));

coilsToLoad = fitParam.channelToPlot;

for coilDx = 1:numel(coilsToLoad)
      for serDx=1:numel(seriesList)
%            for serDx=1:5

        ss{serDx} = Spectro.Spec(seriesList(serDx).instance([seriesList(serDx).instance.InstanceNumber]==coilsToLoad));
        allSpec_Temp(:,:,:,:,coilDx, serDx) = double(ss{serDx}.spectra{1});
        for iDx = 1:length(allSpec_Temp(1,:,1,1,1,1,1))
            for jDx = 1:length(allSpec_Temp(1,1,:,1,1,1,1))
                for slDx = 1:length(allSpec_Temp(1,1,1,:,1,1,1))
            allSpec_Temp_apod(:,iDx,jDx,slDx,coilDx, serDx) = specApodize(ss{serDx}.timeAxis, squeeze(allSpec_Temp(:,iDx,jDx,slDx,coilDx, serDx)), 60);    
                end
            end
        end
        allSpec(:,:,:,:,coilDx, serDx) = allSpec_Temp_apod(:,:,:,:,coilDx, serDx); %only 4th slice for quicker runs
        CallSpec{1,1} = allSpec(:,:,:,:,coilDx,serDx);
        ss{serDx}.overrideSpectra(CallSpec);

        allVolt_V(serDx) = ss{serDx}.getTxSpecPulsePeakVoltage('ss_rf_ec_hard');
        allDuration_ms(serDx) = ss{serDx}.info{1}.csa.MixingTime / 1000;
        
        allSpecPPMaxis(:,serDx) = ss{serDx}.ppmAxis %hmis4 added for plotting spectra
    end
end


fitRes.ss1 = ss{1};
fitRes.allSpec = allSpec;
fitRes.allVolt_V = allVolt_V;
fitRes.allDuration_ms = allDuration_ms;
fitRes.ppmAxis = allSpecPPMaxis;

%%
zeroOrderPhase = sum(allSpec(:,1,1,1),1); % 1st coil in 1st set
zeroOrderPhase = conj(zeroOrderPhase) / abs(zeroOrderPhase);

fitRes.zeroOrderPhase = zeroOrderPhase;

%% Use OXSA for fitting
% Cribbed from D:\Oxford\reports\31p_at_7t\Birdcage31P\matlab\2016-09-23 blochSiegert_in_vivo\fitBlochSiegertBodyCoil_20160923.m

% % Fit with AMARES
% % This is cribbed from https://cardiosvn.fmrib.ox.ac.uk/viewvc/crodgers/people/will/MATLAB/will/CK_QuadCoil/Ck20151127_LoopCoilFAST_Cardiac8.m?revision=9013&view=markup
%prior.pk = AMARES.priorKnowledge.PK_7T_Cardiac();
% % Remove all phase constraints.
% for iDx = 1:numel(prior.pk.priorKnowledge)
%     prior.pk.priorKnowledge(iDx).G_phase = [];
%     prior.pk.priorKnowledge(iDx).refPeak = 0;
% end
prior.pk = AMARES.priorKnowledge.PK_CTR_fids180_SinglePeak();
prior.pk.initialValues.linewidth = 10;
beginTime = 0;
centralFreqOffset = 0;

% fit{1} = AMARES.amaresDriver(ss{serDx},'type','all','plot',true,'prior',prior,'beginTime',beginTime,'expectedOffset',centralFreqOffset,'forceFitAgain',true,'MaxIter',200);

for serDx=1:numel(seriesList)
    %extract the file name of each DICOMS file that we are extracting the
    %spectrum from //hmis4
    fitRes.names{serDx} = seriesList(serDx).SeriesDescription
%    for serDx=1:5

%     if serDx==1 || serDx==numel(seriesList)
%         bPlot = true;
%     else
%         bPlot = false;
%     end
    bPlot = 999; % Plot in here
%     spectraToFit = ss{1}.spectra{1}; %JKA fit only a spectrum from one pixel
%    ss{1}.spectra{1} = allSpec; %JKA fit only a spectrum from one pixel
   
    fitRes.amares{serDx} = AMARES.amaresDriver(ss{serDx},'type','slice','slices',4,'plot',bPlot,'prior',prior,'beginTime',beginTime,'expectedOffset',centralFreqOffset,'forceFitAgain',true,'MaxIter',200);
    
    % Save plot summary to file
    basename = regexprep(fitParam.name,'[^A-Za-z0-9]','_');
    basepath = [mfilename('fullpath') '_figs'];
    
    [~,~] = mkdir(basepath); % Suppress warning if folder exists.
    
    print(fullfile(basepath,[basename '_serDx' num2str(serDx) '.png']),'-dpng','-r300',['-f' num2str(999)])
    %         saveas(figDx,fullfile(basepath,[basename '_fig' num2str(figDx) '.fig']),'fig')
    
end

fitRes.amaresExtract = [];
for serDx=1:numel(seriesList)
%     for serDx=1:5
   
    fitRes.amaresExtract.Amp{serDx} = fitRes.amares{serDx}{1}.dataByPeak.Peak1.Amplitudes;
    fitRes.amaresExtract.Pha{serDx} = fitRes.amares{serDx}{1}.dataByPeak.Peak1.Phases;
   fitRes.amaresExtract.PhaWrapped{serDx} = mod(fitRes.amaresExtract.Pha{serDx} - fitRes.amaresExtract.Pha{1},360);
    for ki = 1:length(fitRes.amaresExtract.PhaWrapped{1, 1})
    if fitRes.amaresExtract.PhaWrapped{serDx}(ki) < 90 || fitRes.amaresExtract.PhaWrapped{serDx}(ki) > 270
        fitRes.amaresExtract.AmpSigned{serDx}(ki) = +fitRes.amaresExtract.Amp{serDx}(ki);
    else
        fitRes.amaresExtract.AmpSigned{serDx}(ki) = -fitRes.amaresExtract.Amp{serDx}(ki);
    end
    end
end
