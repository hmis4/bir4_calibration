voltage = load('voltage_HSn_27012022.mat')
amplitude = load('amplitude_HSn_27012022.mat')

voltage = cell2mat(struct2cell(voltage));
amplitude = cell2mat(struct2cell(amplitude));
pulseduration = 9216e-6; %pulse duration in seconds

hold on
plot(voltage, squeeze(amplitude(1,1,:)))

options = optimoptions('lsqcurvefit')
options.MaxFunctionEvaluations = 30000
params = lsqcurvefit(@(params, voltage) flashSignalEq(params, voltage), [90 1 1], voltage, squeeze(amplitude(3,4,:)).',1,4,options)

plot(linspace(0,500,100), flashSignalEq(params,linspace(0,500,100)))
xlabel('voltage')
ylabel('signal')
hold off


%B1_mapAllHz_V = params(1)*(pi/180)/(2*pi*pulseduration)