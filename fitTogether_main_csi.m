%% Tue 17 Aug 2021.
% NICI coil + body phantom (with water + 78g NaCl and NO 31P) + PPA phantom

% Sweeping RF pulse duration.

if ~exist('Spectro.Spec','class')
    disp('We need RodgersSpectroTools for this code.')
    run('C:\Users\hseab\Documents\ProjectIIIMatlabCode\rodgersspectrotools\setSpectroPaths.m') % on CTR28
% run('D:\postDoc\Reconstruction\RodgersSpectroToolsV2-Package\setSpectroPaths.m')
end

%% Scan data tree
dataDir = 'C:\Users\hseab\Documents\Part III Project\Data\Scan 08.03.2022 - Fermi Point\sequences20220308';
dt = Spectro.dicomTree('dir',dataDir);
dt.prettyPrint

% For now don't analyse this data
%dt2 = Spectro.dicomTree('dir','\\ctr28\shared\DICOM\2021-08\20210818_Test_Phantom_NICI_2');
%dt2.prettyPrint

%% Process everything
clear fitParam
clear fitRes
fitParam(1) = struct('regexp','jk_UTE_csi_fid_Rect_5000us_[0-9]+V','name','voltage_amplitude');

%jk_UTE_csi_fid_20210610_res16_3D_ham_400V_Splitter_A1A2A3A4
for fitDx=1:numel(fitParam)
    fprintf('\n\n FITTING SET #%d:\n\n', fitDx);
    fitParam(fitDx).channelToPlot = 4;
    
    fitRes(fitDx) = fitTogether_fitSet_CSI(dt,fitParam(fitDx));
end

save fitTogether_main_DATA_CSI_3.mat fitParam fitRes % save fit data.

%% For BS phase
clear freqoffs
clear BSphase
for i=1:length(fitRes.names)
    freqoffs(i) = str2double(regexp(fitRes.names(i), '(?<=jk_BS_fermi_1msHPTrue_250V_TR1s_fermi_150V_2x2x8_)[0-9]+(?=khz_noUTE)', 'match', 'once'))
    BSphase(i,:,:) = reshape(cell2mat(fitRes.amaresExtract.Pha(i)).', [2,2]);
end

%% For BS phase for negative offsets
%first need to extract the phases using the "Process everything" section
%of the code
shift = length(freqoffs)
for i=1:length(fitRes.names)
    freqoffs(i+shift) = -str2double(regexp(fitRes.names(i), '(?<=jk_BS_fermi_2msHPTrue_250V_TR1s_fermi_150V_2x2x8_)[0-9]+(?=khz_noUTE)', 'match', 'once'))
    BSphase(i+shift,:,:) = reshape(cell2mat(fitRes.amaresExtract.Pha(i)).', [2,2]);
end


%% Plotting BS phase
clear offsort
clear phasesort
clear isrt
[offssort,isrt] = sort(freqoffs)
phasesort = BSphase(isrt,2,2) % make sure plotting the right pixel
figure(20);
hold on
plot(offssort, phasesort, 'x')
xlabel('Frequency Offset/kHz')
ylabel('BSphase')

%% Plotting the raw spectra on the same graph
figure(21);
hold on
for i=1:length(squeeze(fitRes.allSpec(1,1,1,1,1,:)))
    plot(fitRes.ppmAxis(:,i), squeeze(fitRes.allSpec(:,2,2,4,1,i)), 'DisplayName', ['wRF/kHz=', num2str(freqoffs(i))])
end
xlabel('delta/ppm')
ylabel('Signal')
title('BS Mapping Spectra')
legend show


%% Plot amplitude vs voltage
clear voltage
clear amplitude
for i=1:length(fitRes.names)
    voltage(i)=str2double(regexp(fitRes.names(i), '(?<=jk_UTE_csi_fid_blochSiegert_HSN_12288us_)[0-9]+(?=V_Hamm_TR1500)', 'match', 'once'))
    amplitude(:,:,i) = reshape(cell2mat(fitRes.amaresExtract.Amp(i)).', [2,2])
end

%%
save voltage_Hard_08032022_chan4.mat voltage
save amplitude_Hard_0803022_chan4.mat amplitude

% %% Plot results
% % load fitTogether_main_DATA % skip re-fitting
% for fitDx=1:numel(fitRes)
%     fitTogether_plotSet(fitParam(fitDx), fitRes(fitDx), 10*fitDx);
%     drawnow
%     drawnow
% end

