seriesList = dt.search('target','series','return','series','query',@(ser,stu)any(regexp(ser.SeriesDescription,'jk_BS_fermi_1msHPTrue_250V_TR1s_fermi_150V_2x2x8_-4khz_noUTE','once')))
ss1{1} = Spectro.Spec(seriesList.instance([seriesList.instance.InstanceNumber]==25))

allSpec_Temp(:,:,:,:,1, 1) = double(ss{1}.spectra{1}); %coilDx=1, serDx = 1
for iDx = 1:length(allSpec_Temp(1,:,1,1,1,1,1))
    for jDx = 1:length(allSpec_Temp(1,1,:,1,1,1,1))
        for slDx = 1:length(allSpec_Temp(1,1,1,:,1,1,1))
    allSpec_Temp_apod(:,iDx,jDx,slDx,1, 1) = specApodize(ss{1}.timeAxis, squeeze(allSpec_Temp(:,iDx,jDx,slDx,1, 1)), 60);    
        end
    end
end
allSpec(:,:,:,:,1, 1) = allSpec_Temp_apod(:,:,:,:,1, 1); %only 4th slice for quicker runs
CallSpec{1,1} = allSpec(:,:,:,:,1,1);
ss{1}.overrideSpectra(CallSpec);


prior.pk = AMARES.priorKnowledge.PK_CTR_fids180_SinglePeak();
prior.pk.initialValues.linewidth = 10;
beginTime = 0;
centralFreqOffset = 0;

amares = AMARES.amaresDriver(ss1{1},'type','slice','slices',4,'plot',998,'prior',prior,'beginTime',beginTime,'expectedOffset',centralFreqOffset,'forceFitAgain',true,'MaxIter',200);



%%
figure();
plot(chemshift, signal);
xlabel('delta/ppm')
ylabel('signal')